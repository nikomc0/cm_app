Rails.application.routes.draw do

  resources  :xero_sessions

  devise_for :users

  resources :inventory

  root 'home#index'
end
