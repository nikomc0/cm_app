require 'rails_helper'

RSpec.describe InventoryController, type: :controller do
  let(:inventory) {Inventory.create!(
    office: "1",
    desks: 2,
    company: "Test",
    price: 2000
    )}

  describe "POST create" do
    it "increases the number of Inventory by 1" do
      expect{post :create, inventory: {office: '1', desks: 2, company: "Test", price: 2000}}.to change(Inventory, :count).by(1)
    end

    it "assigns the new office to @inventory" do
      post :create, inventory: {office: '1', desks: 2, company: "Test", price: 2000}
      expect(assigns(:inventory)).to eq Inventory.last
    end

    it "redirects ot the new office" do
      post :create, inventory: {office: '1', desks: 2, company: "Test", price: 2000}
      expect(response).to redirect_to Inventory.last
    end
  end

  describe "DELETE destroy" do
    it "deletes the office" do
      delete :destroy, {id: inventory.id}
      count = Inventory.where({id: inventory.id}).size
      expect(count).to eq 0
    end

    it "redirects to inventory index" do
      delete :destroy, {id: inventory.id}
      expect(response).to redirect_to inventory_index_path
    end
  end
end
