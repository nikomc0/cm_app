class CreateInventories < ActiveRecord::Migration

  def change
      create_table :inventories do |t|
      t.string :office
      t.integer :desks
      t.string :company
      t.integer :price
      t.integer :actual

      t.timestamps null: false
    end
  end
end
