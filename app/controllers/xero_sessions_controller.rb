class XeroSessionsController < ApplicationController
  def index
    @contacts = @client.Contact.all
  end

  def initialize
    @client = Xeroizer::PrivateApplication.new(Rails.application.secrets.xero_consumer_key, Rails.application.secrets.xero_consumer_secret, Rails.application.secrets.cert_path)
  end

  def show
    @contact = @client.Contact.find(params[:id])
    @invoices = @client.Invoice.all(:where => 'Contact.Name == @contact.name.ToString() && Type=="ACCREC" && AmountDue <> 0')
  end
end
