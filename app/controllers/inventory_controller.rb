class InventoryController < ApplicationController

  include ActionView::Helpers::NumberHelper

  def index
    @inventory = Inventory.all
  end

  def new
    @inventory = Inventory.new
  end

  def edit
    @inventory = Inventory.find(params[:id])
  end

  def show
    @inventory = Inventory.find(params[:id])
  end

  def update
    @inventory = Inventory.find(params[:id])
    @inventory.assign_attributes(inventory_params)

    if @inventory.save
      redirect_to inventory_index_path
    else
      flash.now[:alert] = "Error saving. Please try again."
      render :edit
    end
  end

  def create
    @inventory = Inventory.new
    @inventory.assign_attributes(inventory_params)

    if @inventory.save
      flash[:notice] = "Office was saved."
      redirect_to @inventory
    else
      flash.now[:alert] = "There was an error saving the office. Please try again."
      render :new
    end
  end

  def destroy
    @inventory = Inventory.find(params[:id])

    if @inventory.destroy
      flash[:notice] = "Office has been deleted."
      redirect_to inventory_index_path
    else
      flash.now[:alert] = "Error deleting office. Please try again."
      render :destroy
    end
  end

  private
  def inventory_params
    params.require(:inventory).permit(:office, :desks, :company, :price, :actual, :location)
  end

  def shortage

  end
end
