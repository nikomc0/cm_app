module InventoryHelper
  def total_sp_value
    29285.00
  end

  def total_soma_value
    15915.00
  end

  def total_berkeley_value
    24250.00
  end

  def total_palo_alto_value
    21390.00
  end

  def total_sp_current_value
    @south_park = Inventory.south_park
    @south_park.sum(:price)
  end

  def total_soma_current_value
    @soma = Inventory.soma
    @soma.sum(:price)
  end

  def total_berkeley_current_value
    @berkeley = Inventory.berkeley
    @berkeley.sum(:price)
  end

  def total_palo_alto_current_value
    @palo_alto = Inventory.palo_alto
    @palo_alto.sum(:price)
  end

  private
  def percent_to_capacity(total_location_value, total_value)
    percent = 100 * total_location_value / total_value
    percent.ceil
  end

  def shortage (actual, price)
    @shortage = (actual - price)
    @shortage
  end
end
