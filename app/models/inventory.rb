class Inventory < ActiveRecord::Base
  default_scope { order('desks ASC') }

  scope :soma, -> { where(location: 'Soma') }
  scope :south_park, -> { where(location: 'South Park') }
  scope :berkeley, -> { where(location: 'Berkeley') }
  scope :palo_alto, -> { where(location: 'Palo Alto') }
end
